package de.appsonair.xlsx

import de.appsonair.csv.measureTime
import java.io.File
import java.time.LocalDate
import java.time.format.DateTimeFormatter

/**
 * apps on air
 *
 * @author Timo Drick
 */
data class DataEntry(
        val date: LocalDate,
        val open: Float,
        val high: Float,
        val low: Float,
        val close: Float,
        val adjClose: Float,
        val volume: Long
)

fun main() {
    val file = File("BTC-USD.xlsx")
    val parser = XLSXParser(DataEntry::class)
    parser.registerAdapter { LocalDate.parse(it, DateTimeFormatter.ofPattern("yyyy-M-d")) }
    println("Loading data...")
    measureTime {
        parser.parse(file).take(10).forEach { println(it) }
    }

}