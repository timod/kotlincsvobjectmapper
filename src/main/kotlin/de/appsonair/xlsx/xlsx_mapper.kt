/**
 * apps on air
 *
 * @author Timo Drick
 */
package de.appsonair.xlsx

import org.apache.poi.ss.usermodel.Row
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import java.io.File
import java.io.FileInputStream
import kotlin.reflect.KClass
import kotlin.reflect.KParameter
import kotlin.reflect.full.primaryConstructor

class XLSXParser<T: Any>(clazz: KClass<T>) {
    private val constructor = requireNotNull(clazz.primaryConstructor) { "Class ${clazz.simpleName} do not has a primary constructor!" }

    private val stringToAny = mutableMapOf<KClass<*>, (String) -> Any>(
            String::class to { data: String -> data },
            Int::class to { data: String -> data.toDouble().toInt() },
            Long::class to { data: String -> data.toDouble().toLong() },
            Float::class to { data: String -> data.toFloat() },
            Double::class to { data: String -> data.toDouble() }
    )

    inline fun <reified T: Any>registerAdapter(noinline adapter: (String) -> T) {
        registerAdapter(T::class, adapter)
    }
    fun <T: Any>registerAdapter(clazz: KClass<T>, adapter: (String) -> T) {
        stringToAny[clazz] = adapter
    }

    fun parse(file: File): Sequence<T> = sequence {
        val fis = FileInputStream(file)
        val workBook = XSSFWorkbook(fis)
        workBook.getSheetAt(0).rowIterator().apply {
            val mapper = analyzeClass(next())
            while (hasNext()) {
                val row = next()
                yield(parseToClass(mapper, row))
            }
        }
        workBook.close()
    }

    private fun analyzeClass(row: Row): Map<KParameter, (Row) -> Any?> {
        val nameToIndexMap = row.toList().associate { cell ->
            cell.stringCellValue.replace("\\s".toRegex(), "").toLowerCase() to cell.columnIndex
        }
        return constructor.parameters.associateWith { param ->
            //Find column
            val i = requireNotNull(nameToIndexMap[param.name?.toLowerCase()])
                { "Column with name ${param.name} not found!" }
            val f = { row:Row ->
                val d = row.getCell(i)
                if (d == null && param.type.isMarkedNullable)
                    null
                else {
                    val factory = requireNotNull(stringToAny[param.type.classifier])
                        { "No adapter for class: ${param.type.classifier} registered!" }
                    factory(d.toString())
                }
            }
            f
        }
    }

    private fun parseToClass(mapper: Map<KParameter, (Row) -> Any?>, row: Row): T {
        val parameters = constructor.parameters.associateWith { checkNotNull(mapper[it]).invoke(row) }
        return constructor.callBy(parameters)
    }
}