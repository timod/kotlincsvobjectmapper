package de.appsonair.csv

import java.io.File
import java.time.LocalDate
import java.time.format.DateTimeFormatter

/**
 * apps on air
 *
 * @author Timo Drick
 */
data class DataEntry(
        val date: LocalDate,
        val open: Float,
        val high: Float,
        val low: Float,
        val close: Float,
        val adjClose: Float,
        val volume: Long
)

fun measureTime(block: () -> Unit) {
    val start = System.currentTimeMillis()
    block()
    val end = System.currentTimeMillis()
    println("Time: %5.2fs".format((end - start)/1000f))
}

fun main() {
    val file = File("BTC-USD.csv")
    val csvParser = CSVParser(DataEntry::class).apply {
        //registerAdapter(LocalDate::class) { LocalDate.parse(it, DateTimeFormatter.ofPattern("yyyy-M-d")) }
        registerAdapter { LocalDate.parse(it, DateTimeFormatter.ofPattern("yyyy-M-d")) }
    }
    println("Loading data...")
    println("Take 2")
    measureTime {
        file.bufferedReader().use {
            csvParser.read(it).take(2).forEach { println(it) }
        }
        //csvParser.parse(file).toList().take(2).forEach { println(it.toString()) }
    }
}
