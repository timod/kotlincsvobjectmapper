/**
 * apps on air
 *
 * Based on this sample code:
 * https://github.com/JetBrains/kotlin-native/tree/master/samples/csvparser
 *
 * @author Timo Drick
 */
package de.appsonair.csv

import java.io.BufferedReader
import java.io.File
import kotlin.reflect.KClass
import kotlin.reflect.KParameter
import kotlin.reflect.full.primaryConstructor

const val UTF8_BOM = '\uFEFF'

class CSVParser<T: Any>(clazz: KClass<T>, private val separator: Char = ',') {
    private val constructor = requireNotNull(clazz.primaryConstructor) { "Class ${clazz.simpleName} do not has a primary constructor!" }

    private val stringToAny = mutableMapOf<KClass<*>, (String) -> Any>(
            String::class to { data: String -> data },
            Int::class to { data: String -> data.toInt() },
            Long::class to { data: String -> data.toLong() },
            Float::class to { data: String -> data.toFloat() },
            Double::class to { data: String -> data.toDouble() }
    )

    inline fun <reified T: Any>registerAdapter(noinline adapter: (String) -> T) {
        registerAdapter(T::class, adapter)
    }
    fun <T: Any>registerAdapter(clazz: KClass<T>, adapter: (String) -> T) {
        stringToAny[clazz] = adapter
    }

    fun read(reader: BufferedReader) = sequence {
        reader.lineSequence().map { parseLine(it) }.iterator().apply {
            val mapper = analyzeClass(next()) // first line is the header with column names
            while (hasNext()) { //iterate over the next lines
                yield(parseToClass(mapper, next())) // return element
            }
        }
    }

    // loads the complete csv file and converts it into a list of data classes
    fun parseCompleteFile(file: File): List<T> = file.bufferedReader().use { read(it).toList() }

    // provides a sequence to convert the csv file line by line (Or load it just partially)
    fun useFile(file: File, block: (Sequence<T>) -> Unit) {
        file.bufferedReader().use { block(read(it)) }
    }


    private fun analyzeClass(columnNames: Array<String>) = constructor.parameters.associateWith { param ->
        val i = columnNames.indexOfFirst { it.replace("\\s".toRegex(), "").toLowerCase() == param.name?.toLowerCase() }
        require(i >= 0) { "Column with name ${param.name} not found!" }
        val f = { columns: Array<String> ->
            val d = columns[i]
            if (d.isEmpty() && param.type.isMarkedNullable)
                null
            else {
                val factory = stringToAny[param.type.classifier]
                requireNotNull(factory) { "No adapter for class: ${param.type.classifier} registered!" }
                factory(d)
            }
        }
        f
    }

    private fun parseToClass(mapper: Map<KParameter, (Array<String>) -> Any?>, columns: Array<String>): T {
        val parameters = constructor.parameters.associateWith { checkNotNull(mapper[it]).invoke(columns) }
        return constructor.callBy(parameters)
    }

    private fun parseLine(line: String): Array<String> {
        val result = mutableListOf<String>()
        val builder = StringBuilder()
        var quotes = 0
        for (ch in line.trimStart(UTF8_BOM)) {
            when {
                ch == '\"' -> {
                    quotes++
                }
                (ch == '\n') || (ch == '\r') -> {
                }
                (ch == separator) && (quotes % 2 == 0) -> {
                    result.add(builder.toString())
                    builder.setLength(0)
                }
                else -> builder.append(ch)
            }
        }
        result.add(builder.toString())
        return result.toTypedArray()
    }
}