# KotlinCsvObjectMapper

This class maps a csv file to a kotlin data class. (Similar to Gson or Moshi)
Tried to keep the code as simple as possible. (Not so many features yet but easy to adapt to your needs)

Also added a second class to support similar mapping to XLSX (Excel) files.
